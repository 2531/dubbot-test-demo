package com.cifaz.test.dubbo.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.cifaz.test.dubbo.api.ITestService;
import com.cifaz.test.dubbo.dto.Request;
import com.cifaz.test.dubbo.dto.UserDto;
import com.cifaz.tools.util.JsonUtil;
import org.apache.commons.lang3.RandomUtils;

@Service
public class TestService implements ITestService {
    public UserDto hi(Long id) {
        System.out.println("参数:" + id);
        UserDto userDto = new UserDto();
        userDto.setUserName("第一个用户");
        userDto.setPasswd(">>>>>>>>>> " + id);

        return userDto;
    }

    public UserDto getUserDto(Request<UserDto> request) {
        System.out.println("参数:" + JsonUtil.toJson(request));
        System.out.println(request.getData());

        UserDto userDto = new UserDto();
        long nextLong = RandomUtils.nextLong();
        userDto.setUserName("第二个用户:" + nextLong);
        userDto.setPasswd(">>>>>>>>>> " + userDto);
        return userDto;
    }
}

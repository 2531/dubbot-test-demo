package com.cifaz.test.dubbo.dto;

import com.cifaz.tools.dto.BaseRequest;
import io.swagger.annotations.ApiModelProperty;

public class Request<T> extends BaseRequest {
    @ApiModelProperty("通用请求数据体")
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

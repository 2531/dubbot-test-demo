package com.cifaz.test.dubbo.api;

import com.cifaz.test.dubbo.dto.Request;
import com.cifaz.test.dubbo.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "接口")
public interface ITestService {
    UserDto hi(Long id);
    @ApiOperation("获取用户信息")
    UserDto getUserDto(Request<UserDto> request);
}

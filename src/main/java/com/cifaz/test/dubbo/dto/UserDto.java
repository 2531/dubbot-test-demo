package com.cifaz.test.dubbo.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserDto implements Serializable {
    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("用户编码")
    private String passwd;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
}
